__author__ = 'adam'

import random

class Data_base:
    def __init__(self):
        self.get_number_of_piel=0
        self.list_of_piel=[]


    def get_all_database(self,line12,line7_35):
        f=open('data_base/piel_data.txt','r')
        data=f.readlines()
        data.pop(0)
        for i in range(len(data)):
            data[i]=data[i].split()
            temp=Pielegniarka()
            temp.numer=data[i][0]
            temp.imie=data[i][1]
            temp.nazwisko=data[i][2]
            temp.rodz_zmiany=data[i][3]
            if temp.rodz_zmiany=='-j':
                temp.godziny_j=int(line7_35.text())
                temp.gddp_j=temp.godziny_j
                temp.gddp_dz=0
                temp.gddp_nz=0
                temp.dniowki_z=0
                temp.nocki_z=0

            elif temp.rodz_zmiany=='-z':
                if int(line12.text())%2:
                    rand=random.randint(2,4)
                    temp.dniowki_z=int(line12.text())/2
                    if not rand%2:
                        temp.dniowki_z+=+1
                else:
                    temp.dniowki_z=int(line12.text())/2

                temp.nocki_z=int(line12.text())-temp.dniowki_z
                temp.gddp_j=0
                temp.gddp_dz=temp.dniowki_z
                temp.gddp_nz=temp.nocki_z
                temp.godziny_j=0
            self.list_of_piel.append(temp)
        f.close()
        return self

    def get_hours_only(self,line12,line7_35):
        for temp in self.list_of_piel:
            if temp.rodz_zmiany=='-j':
                temp.godziny_j=int(line7_35.text())
                temp.gddp_j=temp.godziny_j
                temp.gddp_dz=0
                temp.gddp_nz=0
                temp.dniowki_z=0
                temp.nocki_z=0

            elif temp.rodz_zmiany=='-z':
                temp.gddp_j=0
                temp.gddp_dz=temp.dniowki_z
                temp.gddp_nz=temp.nocki_z
                temp.godziny_j=0

    def fill_label(self,name_label,numb_label,hours_label,hours_to_give):
        name='Imie i Nazwisko\n'
        numb='nr\n'
        hrs='godziny\n'
        hrs_t_g='do przydzielenia\n'
        for piel in self.list_of_piel:
            name+=piel.imie+' '+ piel.nazwisko+'\n'
            numb+=str(piel.numer)+'\n'
            if piel.godziny_j:
                hrs+=str(piel.godziny_j)+' x 7:35'+'\n'
                hrs_t_g+=str(piel.gddp_j)+'\n'
            elif piel.dniowki_z:
                hrs+=str(piel.dniowki_z)+' + '+str(piel.nocki_z)+'\n'
                hrs_t_g+=str(piel.gddp_dz)+' '+str(piel.gddp_nz)+'\n'
            else:
                hrs+='0\n'
                hrs_t_g='0\n'
        name_label.setText(name)
        numb_label.setText(numb)
        hours_label.setText(hrs)
        hours_to_give.setText(hrs_t_g)



    def set_number_of_hours_to_work(self, new_item, previous_item, type):
        new_item=str(new_item).split()
        previous_item=str(previous_item).split()
        if type==0:
            for piel in self.list_of_piel:
                for i in range(len(previous_item)):
                    if piel.numer==previous_item[i]:
                        piel.gddp_j=int(piel.gddp_j)+1
                for i in range(len(new_item)):
                    if piel.numer==new_item[i]:
                        piel.gddp_j=int(piel.gddp_j)-1
        elif type==1:
            for piel in self.list_of_piel:
                for i in range(len(previous_item)):
                    if piel.numer==previous_item[i]:
                        piel.gddp_dz=int(piel.gddp_dz)+1
                for i in range(len(new_item)):
                    if piel.numer==new_item[i]:
                        piel.gddp_dz=int(piel.gddp_dz)-1
        elif type==2:
            for piel in self.list_of_piel:
                for i in range(len(previous_item)):
                    if piel.numer==previous_item[i]:
                        piel.gddp_nz=int(piel.gddp_nz)+1
                for i in range(len(new_item)):
                    if piel.numer==new_item[i]:
                        piel.gddp_nz=int(piel.gddp_nz)-1
        return self

class Pielegniarka:
    def __init__(self):
        self.numer=0
        self.imie='Jan'
        self.nazwisko='Kowalski'
        self.rodz_zmiany='j'
        self.godziny_j=6
        self.dniowki_z=0
        self.nocki_z=0
        self.gddp_j=0
        self.gddp_dz=0
        self.gddp_nz=0
        self.urlop=Urlop()

    def __getitem__(self, item):
        return item
    def show_piel(self):
        print self.numer,self.imie,self.nazwisko,self.rodz_zmiany,self.godziny_j,self.dniowki_z,self.nocki_z

    def check_if_weekend_ends_in_month(self):
        if self.urlop.finish[0]>=self.urlop.start[0]:
            return 1
        return 0

class Urlop:
    def __init__(self):
        self.start=[0,0,0]
        self.finish=[0,0,0]
