__author__ = 'adam'
from Weekend_window_ui import Ui_Window_add_Urlop
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSlot

class Window_add_week(QtGui.QDialog):
    def __init__(self,list_of_workers, numb_of_nurse, parent=None):
        super(Window_add_week, self).__init__(parent)
        self.ui = Ui_Window_add_Urlop()
        self.ui.setupUi(self)
        self.list_of_workers=list_of_workers
        self.numb_of_nurse=numb_of_nurse
        self.fill_list()
        self.ui.list_of_workers.setCurrentRow(0)
        self.ui.finish_date.setDate(QtCore.QDate.currentDate())
        self.ui.start_date.setDate(QtCore.QDate.currentDate())
        self.ui.add_weeken_button.clicked.connect(self.return_date)

    def fill_list(self):
        for piel in self.list_of_workers:
           self.ui.list_of_workers.addItem(QtGui.QListWidgetItem(str(piel.numer) +'   '+ piel.imie+'   '+piel.nazwisko ))

    @pyqtSlot()
    def return_date(self):
        self.numb_of_nurse.append(str(self.ui.list_of_workers.currentItem().text()).split()[0])
        for workers in self.list_of_workers:
            if workers.numer==self.numb_of_nurse[0]:
               workers.urlop.start[0]=(self.ui.start_date.date().day())
               workers.urlop.start[1]=(self.ui.start_date.date().month())
               workers.urlop.start[2]=(self.ui.start_date.date().year())
               workers.urlop.finish[0]=(self.ui.finish_date.date().day())
               workers.urlop.finish[1]=(self.ui.finish_date.date().month())
               workers.urlop.finish[2]=(self.ui.finish_date.date().year())
               break
        self.close()
