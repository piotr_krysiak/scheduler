#coding: utf-8
__author__ = 'adam'

import random
from PyQt4 import QtGui

class generator:
    def __init__(self,days,groups_j,groups_dz,groups_nz):
        self.days=days
        self.groups_j=groups_j.value()
        self.groups_dz=groups_dz.value()
        self.groups_nz=groups_nz.value()

    def input_7_change(self,list_of_nurse,harm_table):
        lista_piel_j=[]
        for nurse in list_of_nurse:
            if nurse.rodz_zmiany=='-j':
                lista_piel_j.append(nurse)
        if not self.check_if_possible_7(lista_piel_j,harm_table):               #######wstawic powiadomienie o błędzie
            return
        random.shuffle(lista_piel_j)
        piel_index=0
        for groups in range(0,self.groups_j):
            for column in range(1,self.days):
                column_txt=''
                active_day=harm_table.item(0,column).text()
                if(active_day=='Sb'or active_day=='Nd'):
                    harm_table.setItem(groups+1,column,QtGui.QTableWidgetItem('-'))
                    continue
                for i in range(int(harm_table.item(groups+1,0).text())):
                    column_txt+=lista_piel_j[piel_index+i].numer+'\n'
                    lista_piel_j[piel_index+i].gddp_j-=1
                harm_table.setItem(groups+1,column,QtGui.QTableWidgetItem(column_txt))
            piel_index+=int(harm_table.item(groups+1,0).text())





    def input_12_change(self,list_of_nurse,harm_table):
            lista_piel_zm_d=[]
            for nurse in list_of_nurse:
                if nurse.rodz_zmiany=='-z':
                    lista_piel_zm_d.append(nurse)
            if self.check_if_possible_12(lista_piel_zm_d,harm_table)[0]==0:            #######wstawic powiadomienie o błędzie
                return
            lista_piel_zm_n=list(lista_piel_zm_d)
            for groups in range(self.groups_j,self.groups_j+self.groups_dz):
                for column in range(1,self.days):
                    column_txt=''
                    for i in range(int(harm_table.item(groups+1,0).text())):
                        piel=self.control_random(lista_piel_zm_d,harm_table,groups,column,column_txt.split(),1)
                        piel.gddp_dz-=1
                        column_txt+=piel.numer+'\n'
                    harm_table.setItem(groups+1,column,QtGui.QTableWidgetItem(column_txt))

            for groups in range(self.groups_j+self.groups_dz,self.groups_j+self.groups_dz+self.groups_nz):
                for column in range(1,self.days):
                    column_txt=''
                    for i in range(int(harm_table.item(groups+1,0).text())):
                        piel=self.control_random(lista_piel_zm_n,harm_table,groups,column,column_txt.split(),2)
                        piel.gddp_nz-=1
                        column_txt+=piel.numer+'\n'
                    harm_table.setItem(groups+1,column,QtGui.QTableWidgetItem(column_txt))

    def check_if_possible_12(self,list_of_piel,harm_table):
        day_check=0
        night_check=0
        day_wish=0
        night_wish=0
        for piel in list_of_piel:
             day_check+=piel.gddp_dz
             night_check+=piel.gddp_nz
        for groups in range(self.groups_j,self.groups_j+self.groups_dz):
            day_wish+=int(harm_table.item(groups+1,0).text())
        for groups in range(self.groups_j+self.groups_dz,self.groups_j+self.groups_dz+self.groups_nz):
            night_wish+=int(harm_table.item(groups+1,0).text())
        if day_check<day_wish*(harm_table.columnCount()-1):
            return (0,'d')
        elif night_check<night_wish*(harm_table.columnCount()-1):
            return (0,'n')
        else:
            return (1,'d')

    def check_if_possible_7(self,list_of_piel,harm_table):
        check=0
        wish=0
        count_days=list_of_piel[0].godziny_j
        for piel in list_of_piel:
            check+=piel.gddp_j
        for groups in range(0,self.groups_j):
            wish+=int(harm_table.item(groups+1,0).text())
        if check<wish*count_days:
            return 0
        else:
            return 1



    def control_random(self,list_of_nurse,harm_table,group_nr,column,column_tab,type):

            while 1:
                piel=random.choice(list_of_nurse)
                if piel.numer in column_tab:
                    continue
                check=0

                for other_group in range(self.groups_j,group_nr):
                    if str(piel.numer) in str(harm_table.item(other_group+1,column).text()).split():
                        check=1
                    elif other_group<group_nr and type==2:
                        if str(piel.numer) in str(harm_table.item(other_group+1,column+1).text()).split():
                            check=1
                if check==1:
                    continue
                if piel.gddp_dz==0 and type==1:
                    list_of_nurse.remove(piel)
                    continue
                elif piel.gddp_nz==0 and type==2:
                    list_of_nurse.remove(piel)
                    continue
                return piel









