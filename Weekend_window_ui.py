# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Weekend_window.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Window_add_Urlop(object):
    def setupUi(self, Window_add_Urlop):
        Window_add_Urlop.setObjectName(_fromUtf8("Window_add_Urlop"))
        Window_add_Urlop.resize(400, 300)
        self.start_date = QtGui.QDateEdit(Window_add_Urlop)
        self.start_date.setGeometry(QtCore.QRect(70, 220, 110, 22))
        self.start_date.setObjectName(_fromUtf8("start_date"))
        self.finish_date = QtGui.QDateEdit(Window_add_Urlop)
        self.finish_date.setGeometry(QtCore.QRect(250, 220, 110, 22))
        self.finish_date.setObjectName(_fromUtf8("finish_date"))
        self.list_of_workers = QtGui.QListWidget(Window_add_Urlop)
        self.list_of_workers.setGeometry(QtCore.QRect(80, 10, 256, 192))
        self.list_of_workers.setObjectName(_fromUtf8("list_of_workers"))
        self.add_weeken_button = QtGui.QPushButton(Window_add_Urlop)
        self.add_weeken_button.setGeometry(QtCore.QRect(260, 250, 101, 31))
        self.add_weeken_button.setObjectName(_fromUtf8("add_weeken_button"))
        self.label = QtGui.QLabel(Window_add_Urlop)
        self.label.setGeometry(QtCore.QRect(30, 220, 21, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Window_add_Urlop)
        self.label_2.setGeometry(QtCore.QRect(220, 220, 21, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))

        self.retranslateUi(Window_add_Urlop)
        QtCore.QMetaObject.connectSlotsByName(Window_add_Urlop)

    def retranslateUi(self, Window_add_Urlop):
        Window_add_Urlop.setWindowTitle(_translate("Window_add_Urlop", "Form", None))
        self.add_weeken_button.setText(_translate("Window_add_Urlop", "dodaj urlop", None))
        self.label.setText(_translate("Window_add_Urlop", "od", None))
        self.label_2.setText(_translate("Window_add_Urlop", "do", None))

