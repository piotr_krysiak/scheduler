
from Widget import set_weekend_for_nurse, check_if_empty
from PyQt4 import QtCore, QtGui
from testing_window_ui import Ui_MainWindow
import sys


class MyForm(QtGui.QMainWindow):


    def __init__(self, parent=None):

        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.tableWidget.setItem(0,0,QtGui.QTableWidgetItem("something"))



if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())



def should_return_if_cell_is_empty():
    assert check_if_empty(MyForm.ui.tableWidget.item(0,0),0,0)==1
    assert check_if_empty(MyForm.ui.tableWidget.item(1,1),1,1)==0
