from Callendar import get_number_of_days_in_month

def test_should_return_number_of_days_in_2015_09():
    assert get_number_of_days_in_month(2015,9) == 30
    assert get_number_of_days_in_month(2015,8) == 31
