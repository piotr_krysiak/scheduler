# -*- coding: utf-8 -*-
__author__ = 'adam'

import sys
import calendar
from PyQt4 import QtGui

def get_number_of_days_in_month(year, month):
    number_of_days_in_week_plus_year_and_month = 9
    return len(calendar.month(year,month).split()) - number_of_days_in_week_plus_year_and_month

def set_callendar(Harm_table,year,month,hrs,mnt,line12,line12hour,line12minutes,line7_35):
    day_dict={0:'Pn',1:'Wt',2:'Sr',3: "Cz",4:'Pi',5:'Sb',6: "Nd"}
    workdays=0
    single_change_time_in_minutes=455
    length_of_long_change=12
    first_column=1
    column_count=get_number_of_days_in_month(year,month) + first_column

    Harm_table.setColumnCount(column_count)
    print
    if column_count==32:
        Harm_table.setHorizontalHeaderItem(31,QtGui.QTableWidgetItem('31'))
    for i in range(1,column_count):
        temp=calendar.weekday(year,month,i)
        Harm_table.setItem(0,i,QtGui.QTableWidgetItem(day_dict[temp]))
        if temp<5:
            workdays+=1
    for i in range(1,Harm_table.rowCount()-1):
        Harm_table.setItem(i,0,QtGui.QTableWidgetItem('0'))
    number_of_working_hours=workdays*7+workdays*35/60
    hrs.setText(str(number_of_working_hours))
    min=single_change_time_in_minutes*workdays-(number_of_working_hours)*60
    mnt.setText(str(min))
    line7_35.setText(str(workdays))
    hr=(number_of_working_hours)/length_of_long_change
    line12.setText(str(hr))
    line12minutes.setText(str(min))
    line12hour.setText(str(number_of_working_hours-hr*length_of_long_change))








