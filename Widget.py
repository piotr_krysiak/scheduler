__author__ = 'adam'
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui
from Callendar import set_callendar
from MainWindow_ui import Ui_MainWindow
from PyQt4.QtCore import pyqtSlot
import time
from pielegniarki import Data_base
from Generator import  generator
from Weekend_window_controll import Window_add_week



class MyForm(QtGui.QMainWindow):


    def __init__(self, parent=None):

        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.Month_Table.setValue(time.localtime(time.time())[1])
        self.ui.Year_table.setValue(time.localtime(time.time())[0])
        self.Refresh_main_slot()

        self.current_value='0'


#---------------------CONNECTIONS----------------------

        self.ui.Month_Table.valueChanged.connect(self.Refresh_main_slot)
        self.ui.Year_table.valueChanged.connect(self.Refresh_main_slot)
        self.ui.spin_il_jedn.valueChanged.connect(self.set_groups_amount_J)
        self.ui.spin_il_dwu_D.valueChanged.connect(self.set_groups_amount_D)
        self.ui.spin_il_dwu_N.valueChanged.connect(self.set_groups_amount_N)
        self.ui.Harm_Table.cellClicked.connect(self.set_current_value)
        self.ui.Harm_Table.cellChanged.connect(self.analize_changes)
        self.ui.Gen_butt.clicked.connect(self.Generate_harm_table)
        self.ui.add_week_butt.clicked.connect(self.add_weekend)
#------------------SLOTS------------------

    @pyqtSlot()
    def Refresh_main_slot(self):
        set_callendar(self.ui.Harm_Table,self.ui.Year_table.value(),self.ui.Month_Table.value(),self.ui.line_hours_of_work,
                      self.ui.line_minutes_of_work,self.ui.line12,self.ui.line12hour,self.ui.line12minutes,self.ui.line7_35)
        self.list_piel=Data_base().get_all_database(self.ui.line12,self.ui.line7_35)
        self.list_piel.fill_label(self.ui.name_label,self.ui.numb_label,self.ui.hours_label,self.ui.hours_to_give_label)

    @pyqtSlot()
    def set_groups_amount_J(self):
        size=self.ui.spin_il_jedn.value()
        if size>self.ui.Harm_Table.rowCount()-self.ui.spin_il_dwu_D.value()-self.ui.spin_il_dwu_N.value()-2:
            self.ui.Harm_Table.insertRow(size)
            a=QtGui.QTableWidgetItem('7:00 - 14:35 '+chr(64+size))
            self.ui.Harm_Table.setVerticalHeaderItem(size,a)
            self.ui.Harm_Table.setItem(size,0,QtGui.QTableWidgetItem('0'))

        else:
            self.ui.Harm_Table.removeRow(size+1)

    @pyqtSlot()
    def set_groups_amount_D(self):
        size=self.ui.spin_il_dwu_D.value()
        if size>self.ui.Harm_Table.rowCount()-self.ui.spin_il_jedn.value()-self.ui.spin_il_dwu_N.value()-2:
            self.ui.Harm_Table.insertRow(size+self.ui.spin_il_jedn.value())
            a=QtGui.QTableWidgetItem('7:00 - 19:00 '+chr(64+size))
            self.ui.Harm_Table.setVerticalHeaderItem(size+self.ui.spin_il_jedn.value(),a)
            self.ui.Harm_Table.setItem(size+self.ui.spin_il_jedn.value(),0,QtGui.QTableWidgetItem('0'))

        else:
            self.ui.Harm_Table.removeRow(size+self.ui.spin_il_jedn.value()+1)

    @pyqtSlot()
    def set_groups_amount_N(self):
        size=self.ui.spin_il_dwu_N.value()
        pom=self.ui.spin_il_dwu_D.value()+self.ui.spin_il_jedn.value()
        if size>self.ui.Harm_Table.rowCount()-pom-2:
            self.ui.Harm_Table.insertRow(size+pom)
            A_in_ASCII=64
            a=QtGui.QTableWidgetItem('19:00 - 7:00 '+chr(A_in_ASCII+size))
            self.ui.Harm_Table.setVerticalHeaderItem(size+pom,a)
            self.ui.Harm_Table.setItem(size+pom,0,QtGui.QTableWidgetItem('0'))
        else:
            self.ui.Harm_Table.removeRow(size+1+pom)
    @pyqtSlot(int,int)
    def set_current_value(self,row,column):
        if column==0:
            return
        if self.ui.Harm_Table.currentItem():
            self.current_value=self.ui.Harm_Table.currentItem().text()
        else:
            self.current_value='0'



    @pyqtSlot(int,int)
    def analize_changes(self,row,column):
        if column==0 or row==0 or row==self.ui.Harm_Table.rowCount()-1:
            return

        if self.ui.Harm_Table.currentColumn()!=column or self.ui.Harm_Table.currentRow()!=row:
            self.set_current_value(row,column)
        type1=self.ui.spin_il_jedn.value()
        type2=type1+self.ui.spin_il_dwu_D.value()
        type3=type2+self.ui.spin_il_dwu_N.value()
        cur_row=self.ui.Harm_Table.currentRow()
        if cur_row>0and cur_row<=type1:
          self.list_piel=self.list_piel.set_number_of_hours_to_work(self.ui.Harm_Table.currentItem().text(),self.current_value,0)
        elif cur_row>type1 and cur_row<=type2:
          self.list_piel=self.list_piel.set_number_of_hours_to_work(self.ui.Harm_Table.currentItem().text(),self.current_value,1)
        elif cur_row>type2 and cur_row<=type3:
          self.list_piel= self.list_piel.set_number_of_hours_to_work(self.ui.Harm_Table.currentItem().text(),self.current_value,2)

        self.list_piel.fill_label(self.ui.name_label,self.ui.numb_label,self.ui.hours_label,self.ui.hours_to_give_label)

    @pyqtSlot()
    def Generate_harm_table(self):
        self.list_piel.get_hours_only(self.ui.line12,self.ui.line7_35)
        gen=generator(self.ui.Harm_Table.columnCount(),self.ui.spin_il_jedn,self.ui.spin_il_dwu_D,self.ui.spin_il_dwu_N)
        gen.input_12_change(self.list_piel.list_of_piel,self.ui.Harm_Table)
        gen.input_7_change(self.list_piel.list_of_piel,self.ui.Harm_Table)

    @pyqtSlot()
    def add_weekend(self):
        numb_of_nurse=[]
        self.a=Window_add_week(self.list_piel.list_of_piel,numb_of_nurse)
        self.a.exec_()
        set_weekend_for_nurse(numb_of_nurse[0],self.ui.Harm_Table,self.list_piel.list_of_piel)



def set_weekend_for_nurse(number_of_nurse, time_table, list_of_nurse):
       first_column=1
       row_with_weekend=time_table.rowCount()-1
       count_of_days_in_month=time_table.columnCount()-first_column
       nurse=None
       for piel in list_of_nurse:
           if number_of_nurse==piel.numer:
               nurse=piel
               break
       last_day=count_of_days_in_month+1
       if nurse.check_if_weekend_ends_in_month():
            last_day=nurse.urlop.finish[0]+1
       for day in range(nurse.urlop.start[0],last_day):
          if check_if_empty(time_table, row_with_weekend,day):
             text_from_cell=time_table.item(row_with_weekend,day).text()
             new_text=text_from_cell+' '+str(nurse.numer)
          else:
             new_text=str(nurse.numer)
          time_table.setItem(row_with_weekend,day,QtGui.QTableWidgetItem(new_text))


def check_if_empty(table,row,column):
      if table.item(row,column) is not None:
         return 1
      return 0







if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = MyForm()
    myapp.show()
    sys.exit(app.exec_())
